from datetime import datetime
import jwt

from . import config

key = config.read_param("TOKEN_PSW", "zarums")
alg = config.read_param("TOKEN_ALG", "HS384")


def encode_token(user_id: int) -> str:
    token = {'iat': datetime.utcnow(), 'user_id': user_id}
    return jwt.encode(token, key, algorithm=alg).decode("ascii")


def decode_token(token: str) -> dict:
    d = {}
    try:
        d = jwt.decode(token, key, algorithm=alg)
        return d if "user_id" in d else None
    except (jwt.ExpiredSignatureError, jwt.InvalidIssuerError,
            jwt.InvalidAudienceError, jwt.InvalidIssuedAtError,
            jwt.InvalidSignatureError, jwt.DecodeError):
        return None
