# zarums server
This is python-based API and WebSocket zarums server

## Idea and implementation
`// TODO`

## Usage
You can run server by running command:
```bash
env \
"zarums_SERVER_SK=uDILU0lb0arCNJNwdh2AL1jwCNHmh4rxTKX9q7l7iAU=" \
"zarums_SERVER_PK=kiSfY0mVNsvoJ4O0DragZgZSdsb3Gik77QQB+ivdNwg=" \
uvicorn --uds zarums.sock main:FAPI_APP
```
Or you can create `.env` file with environment parameters (details on wiki) and run:
```bash
pipenv run uvicorn --uds zarums.sock main:FAPI_APP
```
All of those would be listen on `zarums.sock`. Use that socket later [in nginx](https://www.uvicorn.org/deployment/#running-behind-nginx) or reverse proxy of your choice.

### Environment variables
| Parameter name      | Description  
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| zarums_SERVER_SK    | base64 secret key                                                                                                                                           |
| zarums_SERVER_PK    | base64 public key                                                                                                                                           |
| zarums_TOKEN_PSW    | password for generating JWT                                                                                                                                 |
| zarums_TOKEN_ALG    | algorithm for generating JWT                                                                                                                                |
| zarums_DB_REVISION  | debug variable to add prefix to table names                                                                                                                 |
| zarums_MYSQL_HOST   | MySQL server IP                                                                                                                                             |
| zarums_MYSQL_PORT   | MySQL server port                                                                                                                                           |
| zarums_MYSQL_USER   | MySQL server user                                                                                                                                           |
| zarums_MYSQL_PASS   | MySQL server password                                                                                                                                       |
| zarums_MYSQL_DBNAME | MySQL database name for zarums (must be created with [example_db.sql](https://gitlab.com/yey-foundation/zarums/server-python/-/blob/master/example_db.sql)) |
| zarums_CORS_ORIGINS | see [FastAPI docs](https://fastapi.tiangolo.com/tutorial/cors/)                                                                                             |
| zarums_CORS_METHODS | look for FastAPI docs                                                                                                                                       |
| zarums_CORS_HEADERS | look for FastAPI docs                                                                                                                                       |
