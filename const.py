rsp = {
    403: {"description": "Not authorized"},
    404: {"description": "Not found"},
    422: {"description": "Validation error. Check /docs"},
    429: {"description": "Too much requests"},
    500: {"description": "Some technical teapots on backend"},
    501: {"description": "Private method. Stay tuned for updates"}
}

ROOM__ACL_R = ["mbr"]
ROOM__ACL_RW = ["adm"]
ROOM__ACL_MEMBER = ROOM__ACL_R + ROOM__ACL_RW