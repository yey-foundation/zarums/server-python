SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `usr` (
  `user_id` int(11) NOT NULL,
  `username` varchar(32) CHARACTER SET ascii NOT NULL COMMENT '[a-z][a-z0-9]{4,31}',
  `user_sec` char(120) CHARACTER SET ascii NOT NULL COMMENT 'base64_encode(sc_encrypt(user_pass, user_sec))',
  `user_pub` char(44) CHARACTER SET ascii NOT NULL COMMENT 'base64_encode(user_pub)',
  `keys` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '\'{}\'' COMMENT '{"chat_id": base64(chat_keys_history)}'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `usr`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `nick` (`username`);


ALTER TABLE `usr`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
