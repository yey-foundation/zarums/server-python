import json
from typing import Dict, List
from starlette.websockets import WebSocket
from fastapi import (APIRouter, Header, Body, Path, HTTPException,
                     BackgroundTasks)

from .. import crypto, config
from ..const import rsp
from ..helpers import db
from ..classes.user import User
from ..classes.room import Room, RoomKey, RoomMetadata


DB_REV = config.read_param("DB_REVISION", "")

WEBSOCKETS = {}

ROUTER_PRV = APIRouter()



@ROUTER_PRV.get("/rooms/list",
                name="List available to user rooms",
                description="...")
async def list_rooms(x_uid: int = Header(default=None)):
    user = User(user_id=x_uid, key_pair={})
    user.load()
    return {"details": "Ok", "result": user.get__rooms()}


async def websocket_notify_room(
    room_id,
    sender_id,
    update_section = {},
    event = "new_message"
):
    room = Room(room_id=room_id)
    if room.load():
        for user_id, _ in room.members.items():
            user_id = int(user_id)
            if user_id in WEBSOCKETS:
                for ws in WEBSOCKETS[user_id]:
                    await ws.send_json({
                        "event": event,
                        "room_id": room_id,     # backward
                        "user_id": sender_id,   #  compatibility
                        "update": update_section
                    }, mode="binary")


@ROUTER_PRV.post("/room/create",
                name="Create chat room",
                description="...")
async def room_create(
        x_uid: int = Header(default=None),
        members: Dict[str, str] = Body(...),
        chat_name: str = Body(...)
):
    members_dict = {}
    for user_id, room_key in members.items():
        if isinstance(user_id, str) and isinstance(room_key, str):
            members_dict.update({int(user_id): room_key})
    del members
    room = Room(chat_name=chat_name)
    if room.save() and room.add_members(members_dict) and room.save():
        return {"details": "Ok", "result": room.dict()}
    return {"details": "Fail"}


@ROUTER_PRV.post("/room/rename",
                name="Change room title")
async def room_rename(
    background_tasks: BackgroundTasks,
    x_uid: int = Header(default=None),
    room_id: int = Body(...),
    new_title: str = Body(...)
):
    room = Room(room_id=room_id)
    if room.load():
        room.room_metadata.chat_name = new_title
        if room.save():
            user = User(user_id=x_uid)
            user.load()
            background_tasks.add_task(
                websocket_notify_room,
                room_id, x_uid, # backward compatibility
                update_section={
                    "room_id": room_id, "nick": user.username,
                    "new_title": new_title
                },
                event="new_title"
            )
            return {"details": "Ok", "result": room.dict()}
    return {"details": "Fail"}


@ROUTER_PRV.post("/room/u/invite",
                name="Invite user",
                description="...")
async def room_invite(
        x_uid: int = Header(default=None),
        room_id: int = Body(...),
        user_id: int = Body(...),
        key_str: str = Body(...)
):
    room = Room(room_id=room_id)
    if room.load() and room.add_members({user_id: key_str}) and room.save():
        return {"details": "Ok"}
    else:
        return {"details": "Fail"}


@ROUTER_PRV.post("/room/m/send",
                name="Send message to room",
                description="...")
async def room__send_message(
        background_tasks: BackgroundTasks,
        x_uid: int = Header(default=None),
        room_id: int = Body(...),
        key_id: int = Body(...),
        m_data: str = Body(...)
):
    room = Room(room_id=room_id)
    if room.load() and room.is_user_allowed(x_uid):
        msg = room.send_message(x_uid, key_id, m_data)
        if msg:
            user = User(user_id=x_uid)
            user.load()
            background_tasks.add_task(
                websocket_notify_room, 
                room_id, x_uid, # backward compatibility
                update_section={
                    "room_id": room_id, "key_id": key_id,
                    "nick": user.username, "data": m_data
                },
            )
            return {"details": "Ok"}
    return {"details": "Fail"}


@ROUTER_PRV.post("/room/m/list",
                name="Get room's messages",
                description="...")
async def room__list_messages(
        x_uid: int = Header(default=None),
        room_id: int = Body(...),
        limit: int = Body(default = 10),
        offset: int = Body(default = 0)):
    room = Room(room_id=room_id)
    if room.load() and room.is_user_allowed(x_uid):
        m_list = room.list_messages(limit, offset)
        return {"details": "Ok", "result": m_list}
    return {"details": "Fail"}


@ROUTER_PRV.post("/room/k/add",
                name="Add new key_id",
                description="...")
async def room__add_keyid(
        x_uid: int = Header(...),
        room_id: int = Body(...),
        keys: Dict[str, str] = Body(...)  # already base64'd for each user
):
    room = Room(room_id=room_id)
    if room.load() and room.is_user_allowed(x_uid):
        room__key_id = room.room_metadata.key_id
        if room__key_id < 255 and room__key_id >= 0:
            room__key_id += 1
        elif room__key_id == 255:
            room__key_id = 0
        else:
            raise HTTPException(
                status_code=422, detail=rsp[422]["description"])
        
        db.sql_multidel(DB_REV + "msg", {
            "room_id": room_id, "key_id": room__key_id})
        
        room.room_metadata.key_id = room__key_id
        b = room.save()
        for user_id, key in keys.items():
            user = User(user_id=int(user_id))
            if user.load():
                rk = RoomKey(user.rooms_keys[str(room_id)].encode("ascii"))
                rk.set__key_id(room__key_id, key.encode("ascii"))
                user.rooms_keys.update({str(room_id): str(rk)})
                b = b and user.save()
        
        if b:
            return {"details": "Ok", "key_id": room__key_id}
        return {"details": "Fail"}


@ROUTER_PRV.post("/room/k/current",
                name="Get current key_id",
                description="...")
async def room__get_keyid(
        x_uid: int = Header(...),
        room_id: int = Body(..., embed=True)
):
    room = Room(room_id=room_id)
    if room.load() and room.is_user_allowed(x_uid):
        db_rp = db.sql_multiget(DB_REV + "usr", {
            "user_id": x_uid, "keys": None,
        })
        keys = json.loads(db_rp[0]["keys"])
        key = keys.get(str(room_id), "")
        room_key = RoomKey(key.encode("ascii"))
        return {
            "details": "Ok",
            "result": {
                "key_id": room.room_metadata.key_id,
                "key": room_key.get__key_id(room.room_metadata.key_id)
            }
        }
    raise HTTPException(status_code=404, detail=rsp[404]["description"])


@ROUTER_PRV.post("/room/k/{key_id}",
                name="Get specific key_id",
                description="...")
async def room__get_keyid_n(
        key_id: int,
        x_uid: int = Header(...),
        room_id: int = Body(..., embed=True)
):
    room = Room(room_id=room_id)
    if room.load() and room.is_user_allowed(x_uid):
        db_rp = db.sql_multiget(DB_REV + "usr", {
            "user_id": x_uid, "keys": None,
        })
        keys = json.loads(db_rp[0]["keys"])
        key = keys.get(str(room_id), "")
        room_key = RoomKey(key.encode("ascii"))
        try:
            result_key = room_key.get__key_id(key_id)
            return {
                "details": "Ok",
                "result": {
                    "key_id": key_id,
                    "key": result_key
                }
            }
        except Exception:
            pass
    raise HTTPException(status_code=404, detail=rsp[404]["description"])
