from typing import List
from binascii import Error as BA_Error
from starlette.websockets import WebSocket
from nacl.exceptions import CryptoError, ValueError as NaCl_ValueError
from fastapi import APIRouter, Header, Body, HTTPException

from .. import auth, config, crypto
from ..const import rsp
from ..helpers import db
from ..classes.user import User


S_SK = config.read_param("SERVER_SK")
S_PK = config.read_param("SERVER_PK")

DB_REV = config.read_param("DB_REVISION", "")

WEBSOCKETS = {}

ROUTER_PUB = APIRouter()
ROUTER_PRV = APIRouter()


@ROUTER_PUB.post("/sign_up")
async def reg_user(
        user_str: str = Body(..., alias="username"),
        user_sk: str = Body(..., alias="sk"),
        user_pk: str = Body(..., alias="pk")
):
    try:
        crypto.pc_importPK(user_pk)
    except (BA_Error, NaCl_ValueError):
        raise HTTPException(status_code=422, detail=rsp[422]["description"])
    user_object = User(
        username=user_str, key_pair={"pk": user_pk, "esk": user_sk})
    status = "Ok" if user_object.save() else "Fail"
    resp_dict = {"details": status}
    resp_dict.update({"result": user_object.dict()} if status == "Ok" else {})
    return resp_dict


@ROUTER_PUB.post("/sign_in/keys")
async def give_userkeys(
        user_str: str = Body(..., embed=True, alias="username")
):
    user_object = User(username=user_str, key_pair={})
    if user_object.load():
        return {
            "details": "Ok",
            "s_pk": crypto.bytes_str(
                crypto.pc_encrypt(
                    user_object.key_pair["pk"], S_PK.encode("ascii"))),
            "u_pk": user_object.key_pair["pk"],  # fix for JS client
            "u_esk": user_object.key_pair["esk"],
        }
    raise HTTPException(status_code=404, detail=rsp[404]["description"])


@ROUTER_PUB.post("/sign_in/token")
async def give_usertoken(
        user_str: str = Body(..., alias="username"),
        c_sq: str = Body(..., alias="checkseq"),
):
    user_object = User(username=user_str, key_pair={})
    if user_object.load():
        try:
            c_sq = crypto.pc_decrypt(S_SK, crypto.str_bytes(c_sq))
        except (TypeError, ValueError, CryptoError):
            raise HTTPException(
                status_code=422, detail=rsp[422]["description"])
        return {
            "details": "Ok",
            "d": crypto.bytes_str(crypto.pc_encrypt(
                user_object.key_pair["pk"], c_sq
            )),
            "token": crypto.bytes_str(crypto.pc_encrypt(
                user_object.key_pair["pk"],
                auth.encode_token(user_object.user_id).encode("ascii")
            )),
        }
    raise HTTPException(status_code=404, detail=rsp[404]["description"])


@ROUTER_PUB.get("/u/{username}",
                name="Get %username% user", description="...")
async def read_user(username: str):
    user_object = User(username=username, key_pair={})
    if user_object.load():
        return {"details": "Ok", "result": user_object.dict()}
    raise HTTPException(status_code=404, detail=rsp[404]["description"])


@ROUTER_PUB.post("/us",
                 name="Get PKs' of multiple users'", description="...")
async def read_users_pk(
        username_list: List[str] = Body(..., embed=True)
):
    result_dict = {}
    for u_s in username_list:
        user_object = User(username=u_s, key_pair={})
        if user_object.load():
            result_dict.update({u_s: user_object.key_pair["pk"]})
    return {"details": "Ok", "result": result_dict}


@ROUTER_PRV.get("/me",
                name="Get authorized user", description="...")
async def read_user_me(
        x_uid: int = Header(default=None)
):
    db_rp = db.sql_multiget(
        DB_REV + "usr", {
            "user_id": x_uid, "username": None,
            "user_sec": None, "user_pub": None
        },
    )
    if len(db_rp) == 1:
        user_object = User(
            user_id=x_uid, username=db_rp[0]["username"], profile_pic="",
            key_pair={"pk": db_rp[0]["user_pub"], "esk": db_rp[0]["user_sec"]}
        )
        return {"details": "Ok", "result": user_object.dict()}
    raise HTTPException(status_code=403, detail=rsp[403]["description"])