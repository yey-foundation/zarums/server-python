import json

from .. import config, crypto
from ..const import ROOM__ACL_MEMBER
from ..helpers import db
from ..helpers.db import __query_read as ll_qr
from ..classes.user import User

DB_REV = config.read_param("DB_REVISION", "")


def u__add_room_key(user_id: int, room_id: int, key: bytes) -> bool:
    result = False
    if not isinstance(key, bytes) or not isinstance(room_id, int):
        raise BaseException("u__add_room_key() - invalid args")
    user = User(user_id=user_id)
    room = Room(room_id=room_id)
    if user.load() and room.load():
        if room_id not in user.rooms_keys:
            user.rooms_keys.update({
                room_id: RoomKey(key)
            })
        result = user.rooms_keys[room_id].set__key_id(
           room.room_metadata.key_id, key)
        result = result and user.save()
    return result

def u__set_room_key(
        user_id: int, room_id: int, key_id: int, key: bytes) -> bool:
    if not isinstance(key, bytes) or not isinstance(room_id, int) \
            or not isinstance(key_id, int):
        raise BaseException("u__set_room_key() - invalid args")
    user = User(user_id=user_id)
    if not user.load() or room_id not in user.rooms_keys:
        raise BaseException("u__set_room_key() - room not found")
    result = user.rooms_keys[room_id].set_keyid(key_id, key)
    return user.save() and result

def u__get_room_key(user_id: int, room_id: int, key_id: int) -> bytes:
    if not isinstance(key_id, int) or not isinstance(room_id, int):
        raise BaseException("u__get_room_key() - invalid args")
    user = User(user_id=user_id)
    if not user.load() or room_id not in user.rooms_keys:
        raise BaseException("u__get_room_key() - room not found")
    return user.rooms_keys[room_id].get_keyid(key_id)

# TODO
def u__del_room_keys(user_id: int, room_id: int) -> bool:
    return False

class RoomKey:
    key_length = 108   # encrypted key length in base64 representation
    ks = bytearray()   # array of 256 encrypted room keys

    def __init__(self, ks: bytes):
        if isinstance(ks, bytes):
            self.ks = bytearray(ks)
        else:
            return None

    def __bytes__(self) -> bytes:
        return bytes(self.ks)

    def __str__(self) -> str:
        return self.__bytes__().decode("ascii")
    __repr__ = __str__

    def get__key_id(self, key_id: int) -> bytes:
        if isinstance(key_id, int):
            buff = self.ks
            start = key_id * self.key_length
            end = start + self.key_length
            k = bytes(buff[start:end])
            if not k:
                raise Exception
            return k
        raise BaseException("RoomKey::get_keyid() - invalid args")

    def set__key_id(self, key_id: int, content: bytes) -> bool:
        if isinstance(key_id, int) and isinstance(content, bytes):
            buff = bytearray(self.ks)
            start = key_id * self.key_length
            end = start + self.key_length
            buff[start:end] = bytearray(content)
            self.ks = bytes(buff)
            return True
        raise BaseException("RoomKey::set_keyid() - invalid args")


class RoomMetadata:
    key_id = 0
    __chat_name = None

    def __getcn(self):
        return self.__chat_name

    def __setcn(self, value):
        self.__chat_name = value

    def __delcn(self):
        del self.__chat_name

    chat_name = property(__getcn, __setcn, __delcn, "Room name")

    def __str__(self):
        return json.dumps(self.__repr__(self))

    def dict(self) -> dict:
        return {"chat_name": self.chat_name, "key_id": self.key_id}

    def load(self, json_str: str) -> bool:
        j = {}
        try:
            j = json.loads(json_str)
        except json.JSONDecodeError:
            raise BaseException("RoomMetadata::load() - invalid args")
        if "chat_name" not in j or "key_id" not in j:
            raise BaseException("RoomMetadata::load() - wrong JSON")
        self.key_id = j["key_id"]
        self.chat_name = j["chat_name"]
        return True


class Room__ID_NOT_FOUND(Exception):
    pass

class Room__EMPTY_MEMBERS_LIST(Exception):
    pass


class Room:
    __room_id = None

    def __getid(self):
        return self.__room_id

    def __setid(self, value):
        self.__room_id = value

    def __delid(self):
        del self.__room_id

    room_id = property(__getid, __setid, __delid, "ID of Room")

    room_metadata = RoomMetadata()

    __members = {}

    def __getml(self):
        return self.__members

    def __setml(self, value):
        self.__members = value

    def __delml(self):
        del self.__members

    members = property(__getml, __setml, __delml, "Room members")

    def __str__(self) -> str:
        return json.dumps(self.__repr__(self))

    def dict(self) -> dict:
        return {
            "room_id": self.room_id, "members": self.members,
            "room_metadata": self.room_metadata.dict()
        }

    def __validator(self, method_name):
        descr = "error occured in " + method_name + "()"
        if self.room_id is None:
            raise Room__ID_NOT_FOUND(descr)
        if len(self.members) == 0:
            raise Room__EMPTY_MEMBERS_LIST(descr)

    def save(self) -> bool:
        try:
            self.__validator("save")
        except Room__ID_NOT_FOUND:
            pass
        result = db.sql_multiupd(
            DB_REV + "room", {
                "rmid": self.room_id, "usrs": json.dumps(self.members),
                "info": json.dumps(self.room_metadata.dict())
            },
        )
        if result:
            self.room_id = db.sql_multiget(
                DB_REV + "room", {
                    "rmid": None,
                    "info": json.dumps(self.room_metadata.dict())
                },
            )[0]["rmid"]
        return result

    def load(self) -> bool:
        try:
            self.__validator("load")
        except Room__EMPTY_MEMBERS_LIST:
            pass
        db_rp = db.sql_multiget(
            DB_REV + "room", {
                "rmid": self.room_id, "info": None, "usrs": None
            }
        )
        if len(db_rp) == 1:
            self.members = json.loads(db_rp[0]["usrs"])
            return self.room_metadata.load(db_rp[0]["info"])
        return False

    def is_user_allowed(self, uid: int, acl: list = ROOM__ACL_MEMBER) -> bool:
        if not isinstance(uid, int):
            raise BaseException("Room::is_user_allowed() - invalid args")
        return self.members.get(str(uid), None) in acl

    def add_members(self, members: dict) -> bool:
        try:
            self.__validator("add_members")
        except Room__EMPTY_MEMBERS_LIST:
            pass
        if not isinstance(members, dict):
            raise BaseException("Room::add_members() - invalid args")
        for user_id, key_str in members.items():
            if u__add_room_key(
                user_id, self.room_id, crypto.str_bytes(key_str)
            ):
                self.members.update({user_id: ROOM__ACL_MEMBER[0]})
        return True

    def del_members(self, members: list) -> bool:
        self.__validator("del_members")
        if not isinstance(members, dict):
            raise BaseException("Room::del_members() - invalid args")
        for user_id in members:
            if user_id in self.members:
                u__del_room_keys(user_id, self.room_id)
                del self.members[user_id]
        return True

    def send_message(self, user_id: int, key_id: int, m_data: str) -> bool:
        self.__validator("del_members")
        if (
            not isinstance(user_id, int) or \
            not isinstance(key_id, int) or \
            not isinstance(m_data, str) or \
            key_id < 0 or key_id > 255 or \
            key_id > self.room_metadata.key_id
        ):
            raise BaseException("Room::send_message() - invalid args")
        result = db.sql_multiupd(
            DB_REV + "msg", {
                "id": None, "room_id": self.room_id, "key_id": key_id,
                "user_id": user_id, "data": m_data
            },
        )
        return result

    #FIXME
    def list_messages(self, limit: int, offset: int) -> list:
        self.__validator("list_messages")
        if (
            not isinstance(limit, int) or \
            not isinstance(offset, int)
        ):
            raise BaseException("Room::list_messages() - invalid args")
        sql = (
            "SELECT * FROM (SELECT `id`,`username` as `nick`,`key_id`,`data` "
            "FROM `%s`, `%s` WHERE `%s`.`user_id`=`%s`.`user_id` "
            "AND `room_id` = %d %s ORDER BY `id` ASC)t "
            "ORDER BY `id` DESC LIMIT " + str(limit)
        )
        args = [
            DB_REV + "usr",
            DB_REV + "msg",
            DB_REV + "usr",
            DB_REV + "msg",
            self.room_id,
            ""
        ]
        args[5] = ("AND `id` < " + str(offset)) if offset > 0 else ""
        return ll_qr(sql % tuple(args), {})[::-1]

    # TODO
    def edit_message(self, key_id: int, m_data: str) -> bool:
        return False

    # TODO
    def delete_message(self, msg_id: int) -> bool:
        return False

    def __init__(self, room_id=None, chat_name="", members={}):
        if not isinstance(members, dict):
            raise BaseException("Room::__init__() - invalid args")
        self.room_id = room_id
        self.members = members
        self.room_metadata.chat_name = chat_name
