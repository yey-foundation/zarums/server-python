import json
from .. import config, crypto
from ..const import ROOM__ACL_MEMBER
from ..helpers import db

DB_REV = config.read_param("DB_REVISION", "")


class JString(json.JSONEncoder):
    def default(self, o):
        return str(o)


class User:
    rooms_keys = {}
    key_pair = {}

    __user_id = None

    def __getid(self):
        return self.__user_id

    def __setid(self, value):
        self.__user_id = value

    def __delid(self):
        del self.__user_id

    user_id = property(__getid, __setid, __delid, "ID of User")

    __username = None

    def __getnm(self):
        return self.__username

    def __setnm(self, value):
        self.__username = value

    def __delnm(self):
        del self.__username

    username = property(__getnm, __setnm, __delnm, "username of User")

    __profile_pic = None

    def __getpp(self):
        return self.__profile_pic

    def __setpp(self, value):
        self.__profile_pic = value

    def __delpp(self):
        del self.__profile_pic

    profile_pic = property(__getpp, __setpp, __delpp, "Profile pic of User")

    def __str__(self):
        return json.dumps(self.__repr__(self))

    def dict(self):
        return {
            "user_id": self.user_id, "username": self.username,
            "profile_pic": self.profile_pic, "key_pair": self.key_pair
        }

    def save(self):
        if (self.username is None or "pk" not in self.key_pair
                or "esk" not in self.key_pair):
            raise BaseException(
                "User::save() - saving invalid object")
        result = db.sql_multiupd(
            DB_REV + "usr", {
                "user_id": self.user_id, "username": self.username,
                "user_sec": self.key_pair.get("esk", ""),
                "user_pub": self.key_pair.get("pk", ""),
                "keys": json.dumps(self.rooms_keys, cls=JString)
            },
        )
        if result:
            self.user_id = db.sql_multiget(
                DB_REV + "usr", {
                    "user_id": self.user_id, "username": self.username
                },
            )[0]["user_id"]
        return result

    def load(self):
        if self.username is None and self.user_id is None:
            raise BaseException(
                "User::load() - no username or user_id")
        db_rp = db.sql_multiget(
            DB_REV + "usr", {
                "user_id": self.user_id, "username": self.username,
                "user_sec": None, "user_pub": None, "keys": None
            }
        )
        if len(db_rp) == 1:
            self.user_id = db_rp[0]["user_id"]
            self.username = db_rp[0]["username"]
            self.rooms_keys = json.loads(db_rp[0]["keys"])
            self.key_pair = {
                "pk": db_rp[0]["user_pub"],
                "esk": db_rp[0]["user_sec"],
            }
            return True
        return False

    def get__rooms(self):
        return db.sql_multiget(DB_REV + "room", {
            "rmid": None, "info": None,
            "u_role": [
                "JSON_UNQUOTE(JSON_EXTRACT(usrs, '$.%d'))" % int(self.user_id),
                "IN",
                ROOM__ACL_MEMBER
            ]
        })

    def __init__(
            self, user_id=None, username=None, key_pair={}, profile_pic=""
    ):
        self.key_pair = key_pair
        self.user_id = user_id
        self.username = username
        self.profile_pic = profile_pic
