import re
from nacl.encoding import Base16Encoder
from mysql.connector import MySQLConnection, Error as MySQL_Error

from .. import config


def __get_dbconfig():
    return {
        "host": config.read_param("MYSQL_HOST", "localhost"),
        "port": config.read_param("MYSQL_PORT", 3306),
        "user": config.read_param("MYSQL_USER"),
        "password": config.read_param("MYSQL_PASS"),
        "database": config.read_param("MYSQL_DBNAME", "zarums"),
    }


def __error_print(e, extra=""):
    s = e._full_msg or str(e)
    print(type(e), ":", s, extra)


def __query_read(sql, data=""):
    rows, conn, cursor = ([], None, None)
    try:
        dbconfig = __get_dbconfig()
        conn = MySQLConnection(**dbconfig)
        cursor = conn.cursor(dictionary=True)
        cursor.execute(sql, data)
        rows = cursor.fetchall()
    except MySQL_Error as e:
        __error_print(e, "Failed to send request to MySQL\nSQL:%s\n" % sql)
    finally:
        try:
            cursor.close()
            conn.close()
        except MySQL_Error:
            print("Failed to close connection to MySQL")
    return rows


def __query_write(sql, data):
    conn, cursor = (None, ) * 2
    try:
        db_config = __get_dbconfig()
        conn = MySQLConnection(**db_config)
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
        return bool(cursor.lastrowid or cursor.rowcount)
    except MySQL_Error as e:
        __error_print(e, "Failed to send request to MySQL\nSQL:%s\n" % sql)
    finally:
        try:
            cursor.close()
            conn.close()
        except MySQL_Error:
            print("Failed to close connection to MySQL")
    return False


def __process_filter(data_dict):
    db_condition, values_place = ("",) * 2
    new_keys = {}
    del_deq = []
    for key in data_dict:
        value = data_dict[key]

        if isinstance(value, str) or isinstance(value, int):
            db_condition += (("`%s`" % key) + " = %("
                             + key + ")s AND ")

        if isinstance(value, list) and len(value) == 1:
            del_deq.append(key)
            values_place += "%s as `%s`, " % (str(value[0]), str(key))
        elif isinstance(value, list) and len(value) == 3:
            if isinstance(value[2], str):
                db_condition += (value[0] + " " + value[1] + " (%("
                                 + key + ")s) AND ")
                data_dict.update({key: value[2]})
            elif isinstance(value[2], list):
                del_deq.append(key)
                db_condition += value[0] + " " + value[1] + " ("
                for element in value[2]:
                    k = Base16Encoder.encode(element.encode("utf-8"))\
                                     .decode("ascii")
                    db_condition += "%(" + k + ")s, "
                    new_keys.update({k: element})
                db_condition = re.sub(r", $", "", db_condition)
                db_condition += ") AND "
            values_place += "%s as `%s`, " % (str(value[0]), str(key))
        else:
            values_place += ("`%s`" % key) + ", "
    data_dict.update(new_keys)
    for key in del_deq:
        del data_dict[key]
    if isinstance(db_condition, str):
        db_condition = re.sub(r" AND $", "", db_condition)
    if isinstance(values_place, str):
        values_place = re.sub(r", $", "", values_place)
    return (db_condition, values_place)


def sql_multiget(group, data_dict):
    if len(data_dict) > 0:
        base_sql = "SELECT %s FROM %s WHERE %s"
        db_condition, values_place = __process_filter(data_dict)
        base_sql = base_sql % (values_place, group, db_condition)
        base_sql = re.sub(r"WHERE $", "", base_sql)
        return __query_read(base_sql, data_dict)
    return False


def sql_multiupd(group, data_dict):
    if len(data_dict) > 0:
        base_sql = "INSERT INTO %s(%s) VALUES(%s) ON DUPLICATE KEY UPDATE %s"
        values_place, values_value, on_duplicate = ("",) * 3
        for key in data_dict:
            values_place += "`" + str(key) + "`, "
            values_value += "%(" + key + ")s, "
            on_duplicate += "`" + str(key) + "`=%(" + str(key) + ")s, "
        values_place = values_place.rstrip(", ")
        values_value = values_value.rstrip(", ")
        on_duplicate = on_duplicate.rstrip(", ")
        return __query_write(
            base_sql % (group, values_place, values_value, on_duplicate),
            data_dict
        )
    return False


def sql_multidel(group, data_dict):
    if len(data_dict) > 0:
        base_sql = "DELETE FROM %s WHERE %s"
        db_condition, _ = __process_filter(data_dict)
        base_sql = base_sql % (group, db_condition)
        base_sql = re.sub(r"WHERE $", "", base_sql)
        return __query_write(base_sql, data_dict)
    return False
