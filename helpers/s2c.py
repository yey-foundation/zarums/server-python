import json
from fastapi import HTTPException

from .. import config, crypto
from ..const import rsp
from ..classes.user import User


def encrypt(uid, r):
    u = User(user_id=uid, kp={})
    u_kp = u.dict()["kp"]
    if "pk" in u_kp and "esk" in u_kp:
        u_pk, u_esk = (u_kp["pk"], u_kp["esk"])
        s_pk = config.read_param("SERVER_PK")
        r["result"] = crypto.pc_encrypt(
            u_pk, json.dumps(r["result"])
        )
        return r
    else:
        raise HTTPException(
            status_code=500, detail=rsp[500]["description"])
