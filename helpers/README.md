**db.py**: High-level interface for working with database engine. Feel free to use this wrapper for your own usage purposes. As now it supports only MySQL :(

Usage examples:
```python
# UPDATE or INSERT
sql_multiupd (
  "user", {
    "id": 1,
    "status": 1,
    "json": '{"one_field": ["one value"], "second": ["another"]}'
})


# SELECT with filter to some fields
# (if you want to only get value from field, set value to None)
sql_multiget (
  "user", {
    "id": 1,
    "status": None, # because status = None, it won't be used for
                    # filtering
    "json": '{"one_field": ["one value"], "second": ["another"]}'
})

# You can ever use JSON_* functions!
# example - you have table like this:
# id  myrow
# 1   {"1": "user"}
# and you want to get this by python
sql_multiget(
  "2001_room", {
    "id": None,
    "my_jsonfilter": [
      "JSON_UNQUOTE(JSON_EXTRACT(myrow, '$.1'))", # your JSON request to db
      "IN", # we use IN operator because there's a list
      ["admin", "user"] # list of acceptable values in myrow[1]
    ]
})


# DELETE using provided filter
sql_multidel (
  "user", {
    "id": 1
})
```
