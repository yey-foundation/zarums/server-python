from os import getenv


def read_param(param_name, default=None):
    return getenv("zarums_" + param_name, default)
