import json
import logging
from fastapi import (
        Depends, FastAPI,
        Header, HTTPException,
        BackgroundTasks
    )
from starlette.requests import Request
from websockets.exceptions import (
        ConnectionClosedError,
        ConnectionClosedOK
    )
from starlette.websockets import (
        WebSocket,
        WebSocketDisconnect
    )
from starlette.middleware.cors import CORSMiddleware

from . import auth
from . import config
from .const import rsp
from .routers import users
from .routers import rooms
from .classes.user import User
from .classes.room import Room
from .routers.rooms import websocket_notify_room


logger = logging.getLogger("fastapi")

FAPI_APP = FastAPI(docs_url=None, redoc_url=None)
FAPI_APP.add_middleware(
    CORSMiddleware,
    max_age=86400,
    allow_credentials=False,
    allow_origins=json.loads(config.read_param(
        "CORS_ORIGINS", "[\"*\"]")),
    allow_methods=json.loads(config.read_param(
        "CORS_METHODS", "[\"POST\", \"GET\", \"OPTIONS\"]")),
    allow_headers=json.loads(config.read_param(
        "CORS_HEADERS", "[\"X-JWT\"]")),
)


WEBSOCKETS = {}
users.WEBSOCKETS = WEBSOCKETS
rooms.WEBSOCKETS = WEBSOCKETS

async def ws_auth(data):
    token = auth.decode_token(data["token"])
    if not isinstance(token, dict) or "user_id" not in token:
        raise Exception
    return token["user_id"]


@FAPI_APP.websocket("/socket")
async def websocket_endpoint(ws: WebSocket):
    try:
        await ws.accept()
        await ws.send_json({"wskv": 0x03251850}, mode="binary")
        user_id = 0
        user = User(user_id=0)
        while True:
            data = await ws.receive_json()
            try:
                if "token" in data:
                    user_id = await ws_auth(data)
                    user.user_id = user_id
                    if not user.load():
                        raise Exception
                else:
                    raise Exception
            except Exception as e:
                await ws.send_json({
                    "event": "auth_rejected"
                }, mode="binary")
                continue
            if (
                data["type"] == "auth" and "token" in data
            ):
                if not user_id == 0:
                    if user_id not in WEBSOCKETS:
                        WEBSOCKETS.update({user_id: [ws]})
                    else:
                        WEBSOCKETS[user_id].append(ws)
                    await ws.send_json({
                        "event": "auth_accepted"
                    }, mode="binary")
            if (
                data["type"] == "room/load" and
                not user_id == 0 and
                "token" in data and
                "room_id" in data and
                "req_id" in data
            ):
                room = Room(room_id=data["room_id"])
                if room.load() and room.is_user_allowed(user_id):
                    data.update(
                        {"limit": 10}
                    ) if "limit" not in data else None
                    data["limit"] = min(abs(data["limit"]), 50)

                    data.update(
                        {"offset": 0}
                    ) if "offset" not in data else None

                    m_list = room.list_messages(
                        data["limit"],
                        data["offset"]
                    )

                    await ws.send_json({
                        "req_id": data["req_id"],
                        "result": m_list
                    }, mode="binary")
            if (data["type"] == "room/send" and 
                not user_id == 0 and
                "token" in data and
                "room_id" in data and
                "key_id" in data and
                "data" in data
            ):
                room = Room(room_id=data["room_id"])
                if (
                    room.load() and 
                    room.send_message(user_id, data["key_id"], data["data"])
                ):
                    await websocket_notify_room(
                        data["room_id"], user_id,
                        update_section={
                            "room_id": data["room_id"],
                            "nick": user.username,
                            "key_id": data["key_id"],
                            "data": data["data"]
                        },
                    )
    except (
        ConnectionClosedError,
        ConnectionClosedOK,
        WebSocketDisconnect
    ):
        logger.debug("Websocket was disconnected")
    except Exception as e:
        logger.warning("Unexpected error: %s", str(e))
    finally:
        for u in WEBSOCKETS:
            if ws in WEBSOCKETS[u]:
                WEBSOCKETS[u].remove(ws)
                logger.debug("Websocket was removed from list")


# FIXME
# changing headers using low-level methods
def modify_reqheaders(request: Request, header: list):
    i = 0
    header = (str(header[0]).lower(), str(header[1]).lower())
    if header[0] in request.headers:
        for lst in request.headers._list:
            if lst[0].decode("ascii") == header[0]:
                lst = (header[0].encode("ascii"), header[1].encode("ascii"))
                request.headers._list.pop(i)
                continue
            i += 1
    if header[1] is not None:
        request.headers._list.append([
            header[0].encode("ascii"), header[1].encode("ascii")
        ])
    return request


@FAPI_APP.middleware("http")
async def jwt_to_uid(request: Request, call_next):
    x_uid = None
    if "x-jwt" in request.headers:
        decoded_token = auth.decode_token(
            request.headers["x-jwt"])
        if (
            decoded_token is not None and
            "user_id" in decoded_token
        ):
            try:
                x_uid = int(
                    decoded_token["user_id"])
            finally:
                pass

    request = modify_reqheaders(
        request,
        ["x-uid", None])
    if x_uid is not None:
        request = modify_reqheaders(
            request,
            ["x-uid", x_uid])

    return await call_next(request)


def jwt_token(x_jwt: str = Header(None)):
    decoded__token = auth.decode_token(x_jwt)
    if decoded__token is not None:
        decoded__user_id = decoded__token["user_id"]
        user = User(user_id=decoded__user_id)
        if user.load():
            return
    raise HTTPException(
        status_code=403,
        detail=rsp[403]["description"])


FAPI_APP.include_router(
    users.ROUTER_PUB,
    prefix="/users"
)

FAPI_APP.include_router(
    users.ROUTER_PRV,
    prefix="/users",
    dependencies=[
        Depends(jwt_token)
    ]
)

FAPI_APP.include_router(
    rooms.ROUTER_PRV,
    dependencies=[
        Depends(jwt_token)
    ]
)
