from nacl import pwhash, secret, utils
from nacl.encoding import Base64Encoder
from nacl.public import PublicKey, PrivateKey, SealedBox, Box


def str_bytes(s: str) -> bytes:
    return Base64Encoder.decode(s)


def bytes_str(b: bytes) -> str:
    return Base64Encoder.encode(b)


# public key cryptography

def pc_genpair() -> dict:
    sk = PrivateKey.generate()
    pk = sk.public_key
    esk = sk.encode(
        encoder=Base64Encoder
    ).decode("ascii")
    epk = pk.encode(
        encoder=Base64Encoder
    ).decode("ascii")
    return {
        "sk": esk,
        "pk": epk
    }


def pc_importPK(s_pk: str) -> PublicKey:
    return PublicKey(s_pk, encoder=Base64Encoder)


def pc_importSK(s_sk: str) -> PrivateKey:
    return PrivateKey(s_sk, encoder=Base64Encoder)


def pc_encrypt(s_pk: str, b_msg: bytes) -> bytes:
    pk = PublicKey(s_pk, encoder=Base64Encoder)
    sb = SealedBox(pk)
    return sb.encrypt(b_msg)


def pc_decrypt(s_sk: str, b_cpt: bytes) -> bytes:
    sk = PrivateKey(s_sk, encoder=Base64Encoder)
    sb = SealedBox(sk)
    return sb.decrypt(b_cpt)


# secret key cryptography

kdf = pwhash.argon2id.kdf
SALT_LEN = pwhash.argon2id.SALTBYTES
KEY_SIZE = secret.SecretBox.KEY_SIZE
NONCE_SIZE = secret.SecretBox.NONCE_SIZE
ops = 1
mem = 8388608


def sc_encrypt(password: bytes, message: bytes) -> bytes:
    salt = utils.random(SALT_LEN)
    Alices_key = kdf(
        KEY_SIZE, password, salt,
        opslimit=ops, memlimit=mem
    )
    Alices_box = secret.SecretBox(Alices_key)
    nonce = utils.random(NONCE_SIZE)

    encrypted = Alices_box.encrypt(message, nonce)
    message = bytearray()
    message += salt
    message += nonce
    message += encrypted.ciphertext

    return bytes(message)


def sc_decrypt(password: bytes, ciphertext: bytes) -> bytes:
    ca = bytearray(ciphertext)

    salt = bytes(ca[0:SALT_LEN])
    nonce = bytes(ca[SALT_LEN:SALT_LEN + NONCE_SIZE])
    message = bytes(ca[SALT_LEN + NONCE_SIZE:])

    Bobs_key = kdf(
        KEY_SIZE, password, salt,
        opslimit=ops, memlimit=mem
    )
    Bobs_box = secret.SecretBox(Bobs_key)
    received = Bobs_box.decrypt(message, nonce)

    return bytes(received)
